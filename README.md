# Iris Flower Classification

### Source : https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data

The data set consists of 50 samples from each of three species of the Iris flowe - Iris setosa, Iris virginica & Iris versicolor 

Four features were measured from each sample: the length and the width of the sepals and petals, in centimetres. 

Based on the combination of these four features, the neural network model is used to distinguish the species from each other.



## Column Metadata

| Column 	 | Description		       | Data Type |
| :-----------:  | :-------------------------: | :-----:   |
| SepalLength cm | Length of the sepal (in cm) | Numeric   |
| SepalWidth  cm | Width of the sepal (in cm)  | Numeric   |
| PetalLength cm | Length of the petal (in cm) | Numeric   |
| PetalWidth  cm | Width of the petal (in cm)  | Numeric   |
| Species        | Species name		       | String    |
