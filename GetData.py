import requests
import sys


def main():
	"""
	This function requests the data through HTTP at the url as specified in the command line argument 
	If no command line argument is specified it uses the default url which is at the UCI Machine Learning Repository 
	"""
	# URL of the data to be downloaded is defined as data_url
	
	if len(sys.argv) > 1:
		data_url = sys.argv[1]
	else:
		data_url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"

	print("Fecthing Data From : \n"+data_url)
	r = requests.get(data_url)

	print("Data Saved in the current directory as iris.csv")
	with open("iris.csv",'wb') as f:
		f.write(r.content)

if __name__ == "__main__" : main()
